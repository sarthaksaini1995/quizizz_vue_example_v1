import Vue from 'vue';
import Router from 'vue-router';

import Home from '../containers/Home/Home.vue';
import Featured from '../containers/Featured/Featured.vue';
import SearchResult from '../containers/SearchResult/SearchResult.vue';

Vue.use( Router );

export default new Router( {
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/featured',
      name: 'featured',
      component: Featured,
      children: [
        {
          path: 'result/:id',
          name: 'search-result',
          component: SearchResult,
        }
      ]
    },
  ]
} );
