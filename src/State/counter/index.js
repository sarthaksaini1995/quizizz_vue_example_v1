import { MutationTypes, ActionTypes } from '../../config';

export const counter = {
  namespaced: true,

  state: {
    value: 0,
    isAnimating: false
  },

  mutations: {
    [ MutationTypes.DECREASE_COUNTER ]( state ) {
      state.value -= 1;
    },
    [ MutationTypes.INCREASE_COUNTER ]( state ) {
      state.value += 1;
    },
    [ MutationTypes.SET_VALUE_TO_COUNTER ]( state, payload ) {
      const val = _.isNumber( payload.toVal ) ? payload.toVal : state.value;
      state.value = val;
    },
    [ MutationTypes.TOGGLE_COUNTER_ANIMATION_STATUS ]( state ) {
      state.isAnimating = !state.isAnimating;
    }
  },

  actions: {
    [ ActionTypes.setValueToCounter ]( { commit, state }, payload ) {
      commit( MutationTypes.SET_VALUE_TO_COUNTER, payload );
    },
    [ ActionTypes.animateCounterToValue ]( { commit, state }, { toVal } ) {
      const originalVal = state.value;
      if ( originalVal === toVal ) {
        return;
      }

      const isValIncreasing = originalVal < toVal;
      commit( MutationTypes.TOGGLE_COUNTER_ANIMATION_STATUS );

      // NEVER do this in an actual action on the actual code; done here for the express purpose to make you see somethings
      const interval = setInterval( () => {
        if ( isValIncreasing ) {
          commit( MutationTypes.INCREASE_COUNTER );
        } else {
          commit( MutationTypes.DECREASE_COUNTER );
        }

        if ( toVal === state.value ) {
          commit( MutationTypes.TOGGLE_COUNTER_ANIMATION_STATUS );
          clearInterval( interval );
        }
      }, 200 );
    }
  }
};
