import Vue from 'vue';
import Vuex from 'vuex';

import { counter } from './counter';
import { featured } from './featured';

Vue.use( Vuex );

export const store = new Vuex.Store( {
  modules: {
    counter,
    featured,
  }
} );

