import _ from 'lodash';

import { ActionTypes, MutationTypes, URLConfig } from '../../config';

export const actions = {
  async [ ActionTypes.fetchFeaturedList ]( { commit, state } ) {
    commit( MutationTypes.FEATURED_LIST_IN_PROGRESS );

    try {
      const response = await fetchList();
      const result = await response.json();
      const cookedResponse = responseCooker( result );

      commit( MutationTypes.FEATURED_LIST_RESPONDED, cookedResponse );
      return cookedResponse;
    } catch ( error ) {
      ( process.env.NODE_ENV !== 'production' ) && console.error( `Error at action ${ActionTypes.fetchFeaturedList}`, error );
    }
  }
};

// Helper Functions
function fetchList() {
  const fetchOptions = {
    method: 'GET',
    credentials: 'same-origin',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    },
  };

  return fetch( URLConfig.getFeaturedSetUrl(), fetchOptions );
}

function responseCooker( response ) {
  return {
    items: _.shuffle( response ),
    totalItems: response.length,
    metaData: _.reduce( response, ( result, item, index ) => {
      response.totalQuestions += item.info.questions.length;
      const n = _.random( 0, item.info.questions.length );
      response.totalMCQQuestions += n;
      response.totalMSQQuestions += item.info.questions.length - n;
      response.totalPlays += item.stats.played;
      response.avgAccuracy += ( _.random( 0, 10 ) / 10 );
    }, {
      totalQuestions: 0,
      totalMCQQuestions: 0,
      totalMSQQuestions: 0,
      totalPlays: 0,
      avgAccuracy: 0
    } )
  };
}
