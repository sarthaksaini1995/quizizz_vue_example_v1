import _ from 'lodash';

import { MutationTypes } from '../../../config';

export const metaData = {
  state: { // Just random practically useless data to explain vuex module chain better
    totalQuestions: -1,
    totalMCQQuestions: -1,
    totalMSQQuestions: -1,
    totalPlays: -1,
    avgAccuracy: -1,
    fetchTime: null
  },

  getters: {
    scoreOfThisSet( state ) {
      if ( state.totalQuestions === -1 ) {
        return 0;
      }

      const { totalMCQQuestions, totalPlays, avgAccuracy } = state;
      return ( 2.5 * totalMCQQuestions ) + ( 1.5 * totalPlays ) * ( avgAccuracy );
    }
  },

  mutations: {
    [ MutationTypes.FEATURED_LIST_RESPONDED ]( state, payload ) {
      state.fetchTime = Date.now();
    }
  }
};
