import _ from 'lodash';

import { list } from './list';
import { metaData } from './metaData';
import { actions } from './actions';

export const featured = {
  modules: {
    list,
    metaData
  },

  actions
};
