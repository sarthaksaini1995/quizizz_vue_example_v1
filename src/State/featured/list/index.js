import _ from 'lodash';

import mutations from './mutations';
import getters from './getters';

export const list = {
  state: {
    items: [],
    totalItems: -1,
    isLoading: false
  },

  getters,

  mutations
};
