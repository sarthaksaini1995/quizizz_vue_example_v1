import _ from 'lodash';

const getters = {
  onlyOddItems( state ) {
    return _.filter( state.items, ( item, index ) => index % 2 === 1 );
  },

  onlyEvenItems( state ) {
    return _.filter( state.items, ( item, index ) => index % 2 === 0 );
  },

  firstNItemsOfTheList: ( state ) => ( n ) => {
    const toIndex = _.isNumber( n ) ? n : 30;
    return _.slice( state.items, 0, toIndex );
  }
};

export default getters;
