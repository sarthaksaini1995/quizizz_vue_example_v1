import _ from 'lodash';

import { MutationTypes } from '../../../config';

const mutations = {
  [ MutationTypes.FEATURED_LIST_IN_PROGRESS ]( state ) {
    state.isLoading = true;
  },
  [ MutationTypes.FEATURED_LIST_RESPONDED ]( state, payload ) {
    state.isLoading = false;
    state.totalItems = payload.totalItems;
    state.items = payload.items;
  }
};

export default mutations;
