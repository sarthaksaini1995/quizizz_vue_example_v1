export const ActionTypes = {
  // counter
  animateCounterToValue: 'animateCounterToValue',
  setValueToCounter: 'setValueToCounter',

  // Featured
  fetchFeaturedList: 'fetchFeaturedList'
};
