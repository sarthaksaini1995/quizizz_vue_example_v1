import _ from 'lodash';

export const URLConfig = {
  getFeaturedSetUrl() {
    return 'https://cf.quizizz.com/game/data/featured/results4.json';
  }
};
