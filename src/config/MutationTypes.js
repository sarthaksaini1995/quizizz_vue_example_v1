export const MutationTypes = {
  // counter
  INCREASE_COUNTER: 'INCREASE_COUNTER',
  DECREASE_COUNTER: 'DECREASE_COUNTER',
  SET_VALUE_TO_COUNTER: 'SET_VALUE_TO_COUNTER',
  TOGGLE_COUNTER_ANIMATION_STATUS: 'TOGGLE_COUNTER_ANIMATION_STATUS',


  // featured
  FEATURED_LIST_IN_PROGRESS: 'FEATURED_LIST_IN_PROGRESS',
  FEATURED_LIST_RESPONDED: 'FEATURED_LIST_RESPONDED',
};
