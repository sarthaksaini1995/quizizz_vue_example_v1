import _ from 'lodash';
import { mapState, mapActions, createNamespacedHelpers } from 'vuex';

import { ActionTypes } from '../../config';

const counterModuleMapper = createNamespacedHelpers( 'counter' );

const Home = {
  name: 'home',

  computed: {
    // imports the whole state of the module as is
    ...mapState( [ 'counter' ] ),
    // Another way th inject the whole module state
    ...mapState( { counterAlias: 'counter' } ),

    ...mapState( {
      counterVal: ( state ) => state.counter.value,
      isCounterAnimating: ( state ) => state.counter.isAnimating
    } ),
    // same state injection just a different way
    ...mapState( 'counter', {
      counterValAlias: 'value',
      isCounterAnimatingAlias: 'isAnimating'
    } ),
    // Again same thing just a different way
    ...counterModuleMapper.mapState( {
      counterValAlias2: 'value',
      isCounterAnimatingAlias2: 'isAnimating'
    } ),

    imgSrc() {
      return '../../assets/logo.png';
    }
  },

  methods: {
    // one way the inject actions of a namespaced module
    ...mapActions( [ `counter/${ActionTypes.setValueToCounter}` ] ),
    ...mapActions( { setValueTo: `counter/${ActionTypes.setValueToCounter}` } ),

    ...mapActions( 'counter', [ ActionTypes.animateCounterToValue ] ),
    ...mapActions( 'counter', { animateToValueAlias: ActionTypes.animateCounterToValue } ),

    ...counterModuleMapper.mapActions( {
      setValueToAlias: ActionTypes.setValueToCounter,
      animateToValueAlias2: ActionTypes.animateCounterToValue
    } ),

    incBy10() {
      if ( this.isCounterAnimating ) {
        return;
      }
      this.setValueTo( { toVal: this.counterVal + 10 } );
    },
    decBy10() {
      if ( this.isCounterAnimating ) {
        return;
      }
      this.setValueTo( { toVal: this.counterVal - 10 } );
    },
    animPlus20() {
      if ( this.isCounterAnimating ) {
        return;
      }
      this.animateToValue( { toVal: this.counterVal + 20 } );
    },
    animMinus20() {
      if ( this.isCounterAnimating ) {
        return;
      }

      this.animateToValue( { toVal: this.counterVal - 20 } );
    },
  }
};

export default Home;
