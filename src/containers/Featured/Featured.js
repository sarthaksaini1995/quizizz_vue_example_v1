import _ from 'lodash';
import { mapActions, mapState, mapGetters } from 'vuex';

import { ActionTypes } from '../../config';
import ListItem from '../../components/ListItem.vue';

export default {
  name: 'featured',
  components: {
    'list-item': ListItem
  },
  data() {
    return {
      mode: 'odd'
    };
  },

  created() {
    this.fetchFeaturedList();
  },

  computed: {
    ...getMappedState(),
    ...getMappedGetters(),
  },

  methods: {
    ...getMappedActions(),
    openListItem( index ) {
      // this.$router(  )
    },

    showOnlyOdd() {
      this.mode = 'odd';
    },
    showOnlyEven() {
      this.mode = 'even';
    },
    showFirst45() {
      this.mode = 'first45';
    },
  }
};

function getMappedGetters() {
  return mapGetters( [
    'onlyOddItems',
    'onlyEvenItems',
    'firstNItemsOfTheList'
  ] );
}

function getMappedState() {
  return mapState( {
    featuredItems: ( state ) => state.featured.list.items,
    isFeaturedLoading: ( state ) => state.featured.list.isLoading,
    featuredMetaData: ( state ) => state.featured.metaData
  } );
}

function getMappedActions() {
  return mapActions( [
    ActionTypes.fetchFeaturedList
  ] );
}
