import Vue from 'vue';

import Root from './containers/Root/Root.vue';
import router from './Router/router';
import { store } from './State/store';

Vue.config.productionTip = false;

new Vue( {
  router,
  store,
  render: ( h ) => h( Root )
} ).$mount( '#root' );
